import * as E from "fp-ts/Either"
import * as O from "fp-ts/Option"
import { run } from "parser-ts/string"

import { pathP } from "../src/parser"

const parse = (s: string) => run(s)(pathP)

describe("parser", () => {
  it("parses URLs without query params", () => {
    const res = parse("/users/:userId")
    expect(res._tag).toEqual("Right")
    if (E.isRight(res)) {
      expect(res.right.value.pathSegments).toEqual(["users", ":userId"])
      expect(res.right.value.query).toEqual(O.none)
    }
  })

  it("parses URLs with query params", () => {
    const res = parse(
      "/users/:userId/preferences/shortcuts/1?foo=aaa&baz=10&prefill=true&values=a,b,c%2Cd",
    )
    expect(res._tag).toEqual("Right")
    if (E.isRight(res)) {
      expect(res.right.value.pathSegments).toEqual([
        "users",
        ":userId",
        "preferences",
        "shortcuts",
        "1",
      ])
      expect(res.right.value.query).toEqual(
        O.some([
          {
            name: "foo",
            value: "aaa",
          },
          {
            name: "baz",
            value: "10",
          },
          { name: "prefill", value: "true" },
          { name: "values", value: "a,b,c%2Cd" },
        ]),
      )
    }
  })

  it("fails to parse invalid URLs", () => {
    expect(parse("/users/alice&bob")._tag).toEqual("Left")
    expect(parse("/users/alice:bob")._tag).toEqual("Left")
    expect(parse("/users/alice@bob")._tag).toEqual("Left")
    expect(parse("/users/alice?bob")._tag).toEqual("Left")
    expect(parse("/users/alice?bob=1&")._tag).toEqual("Left")
  })
})
