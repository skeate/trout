import type { Arbitrary } from "fast-check"
import type { Newtype } from "newtype-ts"

import * as fc from "fast-check"
import * as E from "fp-ts/Either"
import { iso } from "newtype-ts"

import * as tr from "../src"

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface UserIdM
  extends Newtype<
    {
      readonly UserIdM: unique symbol
    },
    string
  > {}
const isoUserId = iso<UserIdM>()

const codecIsIsomorphic = <T>(
  codec: tr.RouteCodec<T>,
  arbT: Arbitrary<T>,
  params?: fc.Parameters<[param: T]>,
) => {
  it("should be isomorphic", () => {
    fc.assert(
      fc.property(arbT, (param) => {
        const encoded = codec.encode(param)
        const decoded = codec.decode(encoded)

        expect(decoded._tag).toBe("Right")
        if (E.isRight(decoded)) {
          expect(decoded.right).toEqual(param)
        }
      }),
      params,
    )
  })
}

const numberArb = fc.oneof(fc.integer(), fc.double()).filter((n) => !Number.isNaN(n))

const userIdArb = fc.string().map(isoUserId.wrap)

describe("route codecs", () => {
  describe("arrayRC", () => {
    codecIsIsomorphic(tr.arrayRC(tr.numberRC), fc.array(numberArb))
  })

  describe("dateRC", () => {
    codecIsIsomorphic(tr.dateRC, fc.date())
  })

  describe("tupleRC", () => {
    codecIsIsomorphic(tr.tupleRC(tr.dateRC, tr.dateRC), fc.tuple(fc.date(), fc.date()))
    codecIsIsomorphic(tr.tupleRC(tr.stringRC, tr.stringRC), fc.tuple(fc.string(), fc.string()))
    codecIsIsomorphic(
      tr.tupleRC(tr.stringRC, tr.stringRC),
      fc.tuple(fc.fullUnicodeString(), fc.fullUnicodeString()),
    )
    codecIsIsomorphic(tr.tupleRC(tr.numberRC, tr.numberRC), fc.tuple(numberArb, numberArb))
    codecIsIsomorphic(tr.tupleRC(tr.dateRC, tr.numberRC), fc.tuple(fc.date(), numberArb))
    codecIsIsomorphic(tr.tupleRC(tr.stringRC, tr.dateRC), fc.tuple(fc.string(), fc.date()))
  })

  describe("booleanRC", () => {
    codecIsIsomorphic(tr.booleanRC, fc.boolean())
  })

  describe("newtypeRC", () => {
    const userIdRC = tr.newtypeRC(isoUserId, tr.stringRC)
    codecIsIsomorphic(userIdRC, userIdArb)
  })

  describe("numberLiteralRC", () => {
    codecIsIsomorphic(tr.numberLiteralRC(5, 3), fc.oneof(fc.constant<5>(5), fc.constant<3>(3)))

    it("should error on non-included numbers", () => {
      const result = tr.numberLiteralRC(5, 3).decode("4")
      expect(result._tag).toEqual("Left")
    })
  })

  describe("numberRC", () => {
    codecIsIsomorphic(tr.numberRC, numberArb)

    it("should error on non-numbers", () => {
      const result = tr.numberRC.decode("a")
      expect(result._tag).toEqual("Left")
    })
  })

  describe("stringLiteralRC", () => {
    codecIsIsomorphic(
      tr.stringLiteralRC("foo", "bar"),
      fc.oneof(fc.constant<"foo">("foo"), fc.constant<"bar">("bar")),
    )

    it("should error on non-included strings", () => {
      const result = tr.stringLiteralRC("foo", "bar").decode("asdf")
      expect(result._tag).toEqual("Left")
    })
  })

  describe("stringRC", () => {
    codecIsIsomorphic(tr.stringRC, fc.string())
  })
})
