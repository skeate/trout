/* eslint-disable @typescript-eslint/ban-ts-comment */
import type { Newtype } from "newtype-ts"

import { expectTypeOf } from "expect-type"
import * as fc from "fast-check"
import * as E from "fp-ts/Either"
import { pipe } from "fp-ts/function"
import * as O from "fp-ts/Option"
import { iso } from "newtype-ts"

import * as trout from "../src"

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface UserIdM
  extends Newtype<
    {
      readonly UserIdM: unique symbol
    },
    string
  > {}
const isoUserId = iso<UserIdM>()

describe("route utilities", () => {
  const userPreferencesRoute = pipe(
    trout.RootPath,
    trout.path("users"),
    trout.param("userId", trout.newtypeRC(isoUserId, trout.stringRC)),
    trout.path("preferences"),
    trout.queryParamOp("baz", trout.numberRC),
    trout.queryParam("foo", trout.stringLiteralRC("aaa", "bbb")),
  )
  const userShortcutsPath = pipe(
    userPreferencesRoute,
    trout.path("shortcuts"),
    trout.param("shortcutId", trout.numberRC),
  )
  const userShortcutsRoute = pipe(
    userShortcutsPath,
    trout.queryParam("prefill", trout.booleanRC),
    trout.queryParam("values", trout.arrayRC(trout.stringRC)),
  )
  const userShortcutsAllOptionalRoute = pipe(
    userShortcutsPath,
    trout.queryParamOp("prefill", trout.booleanRC),
  )

  describe("path", () => {
    it("constructs new paths based on an existing path", () => {
      expect(trout.getRouteString(userPreferencesRoute)).toEqual("/users/:userId/preferences")
    })

    it("clears out any existing query params", () => {
      const newPath = pipe(userShortcutsRoute, trout.path("new"))
      expect(newPath.queryParamCodecs).toEqual({})
    })

    it("decodes the root route", () => {
      const result = trout.decodeUrl(trout.RootPath)("/")
      expect(result._tag).toEqual("Right")
    })
  })

  describe("params", () => {
    describe("encoding", () => {
      it("encodes plain data", () => {
        const url = trout.encodeUrl(userPreferencesRoute)({
          route: {
            userId: isoUserId.wrap("alice"),
          },
          query: {
            baz: O.some(5),
            foo: "aaa",
          },
        })

        expect(url).toEqual("/users/alice/preferences?baz=5&foo=aaa")
      })

      it("encodes optional data", () => {
        const url = trout.encodeUrl(userPreferencesRoute)({
          route: {
            userId: isoUserId.wrap("alice"),
          },
          query: {
            baz: O.none,
            foo: "aaa",
          },
        })

        expect(url).toEqual("/users/alice/preferences?foo=aaa")
      })

      it("encodes array data", () => {
        const url = trout.encodeUrl(userShortcutsRoute)({
          route: {
            shortcutId: 1,
            userId: isoUserId.wrap("alice"),
          },
          query: {
            prefill: true,
            values: ["abc", "def", "ghi", "it's a comma: ,"],
          },
        })

        expect(url).toEqual(
          "/users/alice/preferences/shortcuts/1?prefill=true&values=abc,def,ghi,it's%20a%20comma%3A%20%2C,",
        )
      })

      it("encodes path parameters starting with the same text", () => {
        const pathWithSimilarlyNamedParameters = pipe(
          trout.RootPath,
          trout.path("example"),
          trout.param("foo", trout.stringRC),
          trout.param("foobar", trout.numberRC),
        )

        const url = trout.encodeUrl(pathWithSimilarlyNamedParameters)({
          route: {
            foo: "foo",
            foobar: 5,
          },
        })

        expect(url).toEqual("/example/foo/5")
      })

      it('does not include a "?" if there are no query parameters', () => {
        const emptyRoute = pipe(trout.RootPath)

        const url = trout.encodeUrl(emptyRoute)({})

        expect(url).toEqual("/")
      })

      it('does not include a "?" if there are only empty parameters', () => {
        const emptyRoute = pipe(trout.RootPath, trout.queryParamOp("foo", trout.stringRC))

        const url = trout.encodeUrl(emptyRoute)({
          query: {
            foo: O.none,
          },
        })

        expect(url).toEqual("/")
      })

      it("uses the route codec type in the return type, if it extends string", () => {
        const route = pipe(
          trout.RootPath,
          trout.param("mode", trout.stringLiteralRC("edit", "view")),
        )

        const routeString: "/edit" | "/view" = trout.encodeUrl(route)({
          route: { mode: "edit" },
        })
        expect(routeString).toEqual("/edit")
      })
    })

    it("type-errors if wrong parameter types are supplied", () => {
      trout.encodeUrl(userPreferencesRoute)({
        route: {
          // @ts-expect-error
          userId: "alice",
        },
        query: {
          baz: O.some(5),
          foo: "aaa",
        },
      })

      trout.encodeUrl(userPreferencesRoute)({
        route: {
          userId: isoUserId.wrap("alice"),
        },
        query: {
          baz: O.some(5),
          // @ts-expect-error
          foo: "ccc",
        },
      })

      trout.encodeUrl(userPreferencesRoute)({
        route: {
          userId: isoUserId.wrap("alice"),
        },
        // @ts-expect-error
        query: {
          foo: "aaa",
        },
      })

      trout.encodeUrl(userShortcutsRoute)({
        route: {
          // @ts-expect-error
          shortcutId: "1",
          userId: isoUserId.wrap("alice"),
        },
        query: {
          prefill: true,
          values: ["yes", "no"],
        },
      })

      trout.encodeUrl(userShortcutsRoute)({
        route: {
          shortcutId: 1,
          // @ts-expect-error
          userId: "alice",
        },
        query: {
          prefill: true,
          values: ["yes", "no"],
        },
      })
      trout.encodeUrl(userShortcutsRoute)({
        route: {
          shortcutId: 1,
          userId: isoUserId.wrap("alice"),
        },
        query: {
          // @ts-expect-error
          prefill: "true",
          values: ["yes", "no"],
        },
      })
      trout.encodeUrl(userShortcutsRoute)({
        route: {
          shortcutId: 1,
          userId: isoUserId.wrap("alice"),
        },
        query: {
          prefill: true,
          // @ts-expect-error
          values: [3],
        },
      })
    })

    it("type errors if multiple route parameters share the same name", () => {
      pipe(
        trout.RootPath,
        trout.param("foo", trout.stringRC),
        // @ts-expect-error
        trout.param("foo", trout.numberRC),
      )
    })

    it("errors if not supplied query parameters on a route with no route parameters", () => {
      const routeWithQueryParamsAndNoRouteParams = pipe(
        trout.RootPath,
        trout.queryParam("foo", trout.numberRC),
      )

      const mkUrl = trout.encodeUrl(routeWithQueryParamsAndNoRouteParams)

      // @ts-expect-error
      mkUrl({})

      expect(mkUrl({ query: { foo: 5 } })).toEqual("/?foo=5")
    })

    it("errors if not supplied required route parameters and query parameters", () => {
      const routeWithRouteParamsAndNoQueryParams = pipe(
        trout.RootPath,
        trout.param("userId", trout.stringRC),
        trout.queryParam("foo", trout.numberRC),
      )

      const mkUrl = trout.encodeUrl(routeWithRouteParamsAndNoQueryParams)

      // @ts-expect-error
      mkUrl({})

      // @ts-expect-error
      mkUrl({ route: { userId: "bob" } })

      // @ts-expect-error
      mkUrl({ query: { foo: 5 } })

      expect(mkUrl({ route: { userId: "bob" }, query: { foo: 5 } })).toEqual("/bob?foo=5")
    })

    it("errors if not supplied route parameters on a route with no query parameters", () => {
      const routeWithRouteParamsAndNoQueryParams = pipe(
        trout.RootPath,
        trout.param("userId", trout.stringRC),
      )

      const mkUrl = trout.encodeUrl(routeWithRouteParamsAndNoQueryParams)

      // @ts-expect-error
      mkUrl({})

      expect(mkUrl({ route: { userId: "bob" } })).toEqual("/bob")
    })

    describe("decoding", () => {
      it("decodes plain data", () => {
        const data = trout.decodeUrl(userPreferencesRoute)(
          "/users/alice/preferences?foo=bbb&baz=10",
        )
        expect(data._tag).toEqual("Right")
        if (E.isRight(data)) {
          expect(data.right.routeParams.userId).toEqual("alice")
          expect(data.right.queryParams.foo).toEqual("bbb")
          expect(data.right.queryParams.baz).toEqual(O.some(10))
        }
      })

      it("decodes optional data", () => {
        const data = trout.decodeUrl(userPreferencesRoute)("/users/bob/preferences?foo=bbb")
        expect(data._tag).toEqual("Right")
        if (E.isRight(data)) {
          expect(data.right.routeParams.userId).toEqual("bob")
          expect(data.right.queryParams.foo).toEqual("bbb")
          expect(data.right.queryParams.baz).toEqual(O.none)
        }
      })

      it("decodes array data", () => {
        const data = trout.decodeUrl(userShortcutsRoute)(
          "/users/bob/preferences/shortcuts/1?prefill=true&values=abc,def,ghi,it's%20a%20comma%3A%20%2C,",
        )
        expect(data._tag).toEqual("Right")
        if (E.isRight(data)) {
          expect(data.right.routeParams.userId).toEqual("bob")
          expect(data.right.routeParams.shortcutId).toEqual(1)
          expect(data.right.queryParams.prefill).toEqual(true)
          expect(data.right.queryParams.values).toEqual(["abc", "def", "ghi", "it's a comma: ,"])
        }
      })

      it("decodes optional arrays", () => {
        const route = pipe(
          trout.RootPath,
          trout.queryParamOp("values", trout.arrayRC(trout.stringRC)),
        )

        // The parameter is not provided
        const none = trout.decodeUrl(route)("/")
        expect(none._tag).toEqual("Right")
        if (E.isRight(none)) {
          expect(none.right.queryParams.values._tag).toBe("None")
        }

        // The parameter is provided, but is empty
        const empty = trout.decodeUrl(route)("/?values=")
        expect(empty._tag).toEqual("Right")
        if (E.isRight(empty)) {
          expect(empty.right.queryParams.values._tag).toBe("Some")
          if (O.isSome(empty.right.queryParams.values)) {
            expect(empty.right.queryParams.values.value).toEqual([])
          }
        }

        // The parameter is provided, and has a single comma
        const emptyStr = trout.decodeUrl(route)("/?values=,")
        expect(emptyStr._tag).toEqual("Right")
        if (E.isRight(emptyStr)) {
          expect(emptyStr.right.queryParams.values._tag).toBe("Some")
          if (O.isSome(emptyStr.right.queryParams.values)) {
            expect(emptyStr.right.queryParams.values.value).toEqual([""])
          }
        }
      })

      it("decodes an array with a single empty string", () => {
        const data = trout.decodeUrl(userShortcutsRoute)(
          "/users/bob/preferences/shortcuts/1?prefill=true&values=,",
        )
        expect(data._tag).toEqual("Right")
        if (E.isRight(data)) {
          expect(data.right.routeParams.userId).toEqual("bob")
          expect(data.right.routeParams.shortcutId).toEqual(1)
          expect(data.right.queryParams.prefill).toEqual(true)
          expect(data.right.queryParams.values).toEqual([""])
        }
      })

      it("decodes an array with multiple empty strings", () => {
        const data = trout.decodeUrl(userShortcutsRoute)(
          "/users/bob/preferences/shortcuts/1?prefill=true&values=,,a,,",
        )
        expect(data._tag).toEqual("Right")
        if (E.isRight(data)) {
          expect(data.right.routeParams.userId).toEqual("bob")
          expect(data.right.routeParams.shortcutId).toEqual(1)
          expect(data.right.queryParams.prefill).toEqual(true)
          expect(data.right.queryParams.values).toEqual(["", "", "a", ""])
        }
      })

      it("decodes a parameter that has multiple occurences, taking the last", () => {
        const data = trout.decodeUrl(userShortcutsRoute)(
          "/users/bob/preferences/shortcuts/1?prefill=true&values=xyz,qrs,&prefill=false",
        )
        expect(data._tag).toEqual("Right")
        if (E.isRight(data)) {
          expect(data.right.routeParams.userId).toEqual("bob")
          expect(data.right.routeParams.shortcutId).toEqual(1)
          expect(data.right.queryParams.prefill).toEqual(false)
          expect(data.right.queryParams.values).toEqual(["xyz", "qrs"])
        }
      })

      it("decodes an array parameter that has multiple occurences, taking the last", () => {
        const data = trout.decodeUrl(userShortcutsRoute)(
          "/users/bob/preferences/shortcuts/1?values=abc,def,ghi,it's%20a%20comma%3A%20%2C,&prefill=true&values=xyz,qrs,",
        )
        expect(data._tag).toEqual("Right")
        if (E.isRight(data)) {
          expect(data.right.routeParams.userId).toEqual("bob")
          expect(data.right.routeParams.shortcutId).toEqual(1)
          expect(data.right.queryParams.prefill).toEqual(true)
          expect(data.right.queryParams.values).toEqual(["xyz", "qrs"])
        }
      })

      it("decodes missing query params if all are optional", () => {
        const data = trout.decodeUrl(userShortcutsAllOptionalRoute)(
          "/users/bob/preferences/shortcuts/1",
        )
        expect(data._tag).toEqual("Right")
        if (E.isRight(data)) {
          expect(data.right.routeParams.userId).toEqual("bob")
          expect(data.right.routeParams.shortcutId).toEqual(1)
          expect(data.right.queryParams.prefill).toEqual(O.none)
        }
      })
    })

    describe("decode failure cases", () => {
      it("fails with appropriate error if a route param is invalid", () => {
        const data = trout.decodeUrl(userShortcutsPath)(
          "/users/bob/preferences/shortcuts/zzz?foo=aaa",
        )
        expect(data._tag).toEqual("Left")
        if (E.isLeft(data)) {
          expect(data.left.tag).toEqual("FailedToDecodeRouteParam")
          if (data.left.tag === "FailedToDecodeRouteParam") {
            expect(data.left.routeParam).toEqual("shortcutId")
          }
        }
      })

      it("fails with appropriate error if a query param is invalid", () => {
        const data = trout.decodeUrl(userPreferencesRoute)("/users/bob/preferences?foo=ccc")
        expect(data._tag).toEqual("Left")
        if (E.isLeft(data)) {
          expect(data.left.tag).toEqual("QueryParamFailures")
          if (data.left.tag === "QueryParamFailures") {
            expect(data.left.failures).toHaveLength(1)
            expect(data.left.failures[0].queryParam).toEqual("foo")
          }
        }
      })

      it("fails with appropriate error if path segment counts do not match", () => {
        const data = trout.decodeUrl(userPreferencesRoute)("/userz/bob?foo=bbb")
        expect(data._tag).toEqual("Left")
        if (E.isLeft(data)) {
          expect(data.left.tag).toEqual("SegmentCountMismatch")
          if (data.left.tag === "SegmentCountMismatch") {
            expect(data.left.actualSegments).toEqual(["userz", "bob"])
            expect(data.left.expectedSegments).toEqual(["users", ":userId", "preferences"])
          }
        }
      })

      it("fails with appropriate error if a path segment does not match", () => {
        const data = trout.decodeUrl(userPreferencesRoute)("/userz/bob/preferences?foo=bbb")
        expect(data._tag).toEqual("Left")
        if (E.isLeft(data)) {
          expect(data.left.tag).toEqual("SegmentMismatch")
          if (data.left.tag === "SegmentMismatch") {
            expect(data.left.expected).toBe("users")
            expect(data.left.actual).toBe("userz")
          }
        }
      })

      it("fails with appropriate error if a codec is missing", () => {
        const data = trout.decodeUrl(
          // TODO: This error case does not seem reachable in practice
          pipe(
            userPreferencesRoute,
            trout.param("missing", undefined as unknown as trout.RouteCodec<number>),
          ),
        )("/users/bob/preferences/what?foo=bbb")
        expect(data._tag).toEqual("Left")
        if (E.isLeft(data)) {
          expect(data.left.tag).toEqual("MissingDecoder")
          if (data.left.tag === "MissingDecoder") {
            expect(data.left.routeParam).toBe("missing")
          }
        }
      })

      it("fails with appropriate error if URL fails to parse", () => {
        const data = trout.decodeUrl(userPreferencesRoute)("/userz/bob/preferences&foo=bbb")
        expect(data._tag).toEqual("Left")
        if (E.isLeft(data)) {
          expect(data.left.tag).toEqual("FailedToParseURL")
        }
      })

      it("fails with appropriate error if missing a required query param", () => {
        const data = trout.decodeUrl(userPreferencesRoute)("/users/bob/preferences?bar=bbb")
        expect(data._tag).toEqual("Left")
        if (E.isLeft(data)) {
          expect(data.left.tag).toEqual("QueryParamFailures")
          if (data.left.tag === "QueryParamFailures") {
            expect(data.left.failures[0].tag).toEqual("MissingRequiredQueryParam")
            if (data.left.failures[0].tag === "MissingRequiredQueryParam") {
              expect(data.left.failures[0].queryParam).toBe("foo")
            }
          }
        }
      })

      it("fails with multiple errors if missing a required query param and query param fails to decode", () => {
        const data = trout.decodeUrl(userShortcutsRoute)(
          "/users/bob/preferences/shortcuts/9?prefill=tru",
        )
        expect(data._tag).toEqual("Left")
        if (E.isLeft(data)) {
          expect(data.left.tag).toEqual("QueryParamFailures")
          if (data.left.tag === "QueryParamFailures") {
            expect(data.left.failures).toHaveLength(2)
          }
        }
      })
    })
  })

  describe("getRouteString", () => {
    it("constructs the correct route string", () => {
      const result: "/users/:userId/preferences" = trout.getRouteString(userPreferencesRoute)
      expect(result).toEqual("/users/:userId/preferences")
    })
  })

  describe("prettyPrintFailure", () => {
    it("prints appropriate error if a route param is invalid", () => {
      const data = trout.decodeUrl(userShortcutsPath)(
        "/users/bob/preferences/shortcuts/zzz?foo=aaa",
      )
      expect(data._tag).toEqual("Left")
      if (E.isLeft(data)) {
        const actual = trout.prettyPrintFailure(data.left)
        const expected = [
          'Failure parsing route param "shortcutId":',
          '  cannot decode "zzz", should be number',
        ].join("\n")

        expect(actual).toEqual(expected)
      }
    })

    it("prints appropriate error if a query param is invalid", () => {
      const data = trout.decodeUrl(userPreferencesRoute)("/users/bob/preferences?foo=ccc")
      expect(data._tag).toEqual("Left")
      if (E.isLeft(data)) {
        const actual = trout.prettyPrintFailure(data.left)
        const expected = [
          'Failure parsing query param "foo":',
          '  cannot decode "ccc", should be "aaa" | "bbb"',
        ].join("\n")

        expect(actual).toEqual(expected)
      }
    })

    it("prints appropriate error if path segment counts do not match", () => {
      const data = trout.decodeUrl(userPreferencesRoute)("/userz/bob?foo=bbb")
      expect(data._tag).toEqual("Left")
      if (E.isLeft(data)) {
        const actual = trout.prettyPrintFailure(data.left)
        const expected = "Expected (3) segments, received (2)"

        expect(actual).toEqual(expected)
      }
    })

    it("prints appropriate error if a path segment does not match", () => {
      const data = trout.decodeUrl(userPreferencesRoute)("/userz/bob/preferences?foo=bbb")
      expect(data._tag).toEqual("Left")
      if (E.isLeft(data)) {
        const actual = trout.prettyPrintFailure(data.left)
        const expected = 'Expected segment to match "users", received "userz"'

        expect(actual).toEqual(expected)
      }
    })

    it("prints appropriate error if a codec is missing", () => {
      const data = trout.decodeUrl(
        // TODO: This error case does not seem reachable in practice
        pipe(
          userPreferencesRoute,
          trout.param("missing", undefined as unknown as trout.RouteCodec<number>),
        ),
      )("/users/bob/preferences/what?foo=bbb")
      expect(data._tag).toEqual("Left")
      if (E.isLeft(data)) {
        const actual = trout.prettyPrintFailure(data.left)
        const expected = 'Missing decoder for route param "missing"'

        expect(actual).toEqual(expected)
      }
    })

    it("prints appropriate error if URL fails to parse", () => {
      const data = trout.decodeUrl(userPreferencesRoute)("/userz/bob/preferences&foo=bbb")
      expect(data._tag).toEqual("Left")
      if (E.isLeft(data)) {
        const actual = trout.prettyPrintFailure(data.left)
        const expected = [
          "Failure parsing URL:",
          "  /userz/bob/preferences&foo=bbb",
          '                        ⌃ Expected "?" or end of URL',
        ].join("\n")

        expect(actual).toEqual(expected)
      }
    })

    it("prints appropriate truncated error if long URL fails to parse", () => {
      const data = trout.decodeUrl(userPreferencesRoute)(
        "/very/longgggggggggggggggggggggggggggggggggggggg/userz/bob/preferences&foo=bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb",
      )
      expect(data._tag).toEqual("Left")
      if (E.isLeft(data)) {
        const actual = trout.prettyPrintFailure(data.left)
        const expected = [
          "Failure parsing URL:",
          "  …ggggggggggggggggg/userz/bob/preferences&foo=bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb…",
          '                                          ⌃ Expected "?" or end of URL',
        ].join("\n")

        expect(actual).toEqual(expected)
      }
    })

    it("prints appropriate error if missing a required query param", () => {
      const data = trout.decodeUrl(userPreferencesRoute)("/users/bob/preferences?bar=bbb")
      expect(data._tag).toEqual("Left")
      if (E.isLeft(data)) {
        const actual = trout.prettyPrintFailure(data.left)
        const expected = 'Missing required query param "foo"'

        expect(actual).toEqual(expected)
      }
    })

    it("prints multiple errors if missing a required query param and query param fails to decode", () => {
      const data = trout.decodeUrl(userShortcutsRoute)(
        "/users/bob/preferences/shortcuts/9?prefill=tru",
      )
      expect(data._tag).toEqual("Left")
      if (E.isLeft(data)) {
        const actual = trout.prettyPrintFailure(data.left)
        const expected = [
          'Failure parsing query param "prefill":',
          '  cannot decode "tru", should be "true" | "false"',
          "",
          'Missing required query param "values"',
        ].join("\n")

        expect(actual).toEqual(expected)
      }
    })
  })

  describe("trout properties", () => {
    const optionArb = <T>(arb: fc.Arbitrary<T>): fc.Arbitrary<O.Option<T>> =>
      fc.oneof(fc.constant(O.none), arb.map(O.some))

    const exampleUrl = pipe(
      trout.RootPath,
      trout.path("example"),
      trout.path("string"),
      trout.param("str", trout.stringRC),
      trout.path("stringLiteral"),
      trout.param("strLit", trout.stringLiteralRC("foo", "bar")),
      trout.path("num"),
      trout.param("num", trout.numberRC),
      trout.path("numLit"),
      trout.param("numLit", trout.numberLiteralRC(1, 3, 5)),
    )

    // split out just because of the 20-arg-limit to pipe
    const exampleRoute = pipe(
      exampleUrl,
      trout.queryParam("str", trout.stringRC),
      trout.queryParamOp("strOp", trout.stringRC),
      trout.queryParam("strArray", trout.arrayRC(trout.stringRC)),
      trout.queryParam("strLit", trout.stringLiteralRC("foo", "bar")),
      trout.queryParamOp("strLitOp", trout.stringLiteralRC("foo", "bar")),
      trout.queryParam("strLitArray", trout.arrayRC(trout.stringLiteralRC("foo", "bar"))),
      trout.queryParam("num", trout.numberRC),
      trout.queryParamOp("numOp", trout.numberRC),
      trout.queryParam("numArray", trout.arrayRC(trout.numberRC)),
      trout.queryParam("numLit", trout.numberLiteralRC(1, 3, 5)),
      trout.queryParamOp("numLitOp", trout.numberLiteralRC(1, 3, 5)),
      trout.queryParam("numLitArray", trout.arrayRC(trout.numberLiteralRC(1, 3, 5))),
    )

    const foobar = fc.oneof(fc.constant<"foo">("foo"), fc.constant<"bar">("bar"))

    const numberArb = fc.oneof(fc.integer(), fc.double()).filter((n) => !Number.isNaN(n))

    const oddNumLit = fc.oneof(fc.constant<1>(1), fc.constant<3>(3), fc.constant<5>(5))

    const paramsArb = fc.record({
      routeParams: fc.record({
        str: fc.string(),
        strLit: foobar,
        num: numberArb,
        numLit: oddNumLit,
      }),
      queryParams: fc.record({
        str: fc.string(),
        strOp: optionArb(fc.string()),
        strArray: fc.array(fc.string()),
        strLit: foobar,
        strLitOp: optionArb(foobar),
        strLitArray: fc.array(foobar),
        num: numberArb,
        numOp: optionArb(numberArb),
        numArray: fc.array(numberArb),
        numLit: oddNumLit,
        numLitOp: optionArb(oddNumLit),
        numLitArray: fc.array(oddNumLit),
      }),
    })

    it("encode/decode are isomorphic", () => {
      fc.assert(
        fc.property(paramsArb, (params) => {
          const encoded = trout.encodeUrl(exampleRoute)({
            route: params.routeParams,
            query: params.queryParams,
          })

          const result = trout.decodeUrl(exampleRoute)(encoded)
          expect(result._tag).toBe("Right")

          if (E.isRight(result)) {
            expect(result.right.routeParams).toEqual(params.routeParams)
            expect(result.right.queryParams).toEqual(params.queryParams)
          }
        }),
      )
    })
  })

  describe("query param types", () => {
    describe("encoding", () => {
      const noQPs = pipe(trout.RootPath, trout.path("test"))
      const requiredQPs = pipe(noQPs, trout.queryParam("foo", trout.numberRC))
      const optionalQPs = pipe(noQPs, trout.queryParamOp("foo", trout.numberRC))
      const mixedQPs = pipe(
        noQPs,
        trout.queryParam("foo", trout.numberRC),
        trout.queryParamOp("bar", trout.numberRC),
      )
      it("shows a question mark based on query param requirements", () => {
        const encodedNone = trout.encodeUrl(noQPs)({})
        expectTypeOf(encodedNone).toEqualTypeOf<`/test`>()

        const encodedRequired = trout.encodeUrl(requiredQPs)({ query: { foo: 1 } })
        expectTypeOf(encodedRequired).toEqualTypeOf<`/test?${string}`>()

        const encodedMixed = trout.encodeUrl(mixedQPs)({ query: { foo: 1, bar: O.some(1) } })
        expectTypeOf(encodedMixed).toEqualTypeOf<`/test?${string}`>()

        const encodedOptional = trout.encodeUrl(optionalQPs)({ query: { foo: O.some(1) } })
        expectTypeOf(encodedOptional).toEqualTypeOf<`/test?${string}` | `/test`>()
      })
    })
  })
})
