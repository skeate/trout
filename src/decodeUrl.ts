/* eslint-disable @typescript-eslint/no-explicit-any */
import type {
  QueryParamCodec,
  QueryParamCodecs,
  QueryParametersForRoute,
  RouteDefinition,
  RouteParametersForRoute,
} from "./route"
import type { RouteCodec } from "./routeCodec"
import type { DecodeError } from "io-ts/Decoder"
import type { ParseError } from "parser-ts/ParseResult"

import * as E from "fp-ts/Either"
import { flow, pipe } from "fp-ts/function"
import * as O from "fp-ts/Option"
import * as RA from "fp-ts/ReadonlyArray"
import * as RNEA from "fp-ts/ReadonlyNonEmptyArray"
import * as RR from "fp-ts/ReadonlyRecord"
import * as Sg from "fp-ts/Semigroup"
import { draw } from "io-ts/Decoder"
import { run } from "parser-ts/string"

import { pathP } from "./parser"

type DecodedRoute<Route> = E.Either<RouteDecodeFailure, RouteDecodeSuccess<Route>>

export type RouteDecodeFailure =
  | FailedToParseURL
  | QueryParamFailures
  | SegmentCountMismatch
  | SegmentMismatch
  | MissingDecoder
  | FailedToDecodeRouteParam

export type RouteDecodeSuccess<Route> = Route extends RouteDefinition<any, any, any>
  ? {
      routeParams: RouteParametersForRoute<Route>
      queryParams: QueryParametersForRoute<Route>
    }
  : never

type FailedToParseURL = {
  readonly tag: "FailedToParseURL"
  readonly error: ParseError<string>
}

export const decodeUrl =
  <
    RSs extends Array<string>,
    RPs extends Partial<Record<string, RouteCodec<any>>>,
    QPs extends QueryParamCodecs<any>,
    R extends RouteDefinition<RSs, RPs, QPs>,
  >(
    route: R,
  ) =>
  (s: string): DecodedRoute<R> =>
    pipe(
      E.Do,
      E.apS(
        "givenUrl",
        pipe(
          run(s)(pathP),
          E.mapLeft((error) => ({ tag: "FailedToParseURL" as const, error })),
          E.map(({ value }) => value),
        ),
      ),
      E.bindW("decodedRouteParams", ({ givenUrl: { pathSegments } }) =>
        decodeRouteParams(route)(pathSegments),
      ),
      E.bind("remappedQueryParams", ({ givenUrl: { query } }) =>
        E.right(
          RR.fromFoldableMap(Sg.last<string>(), RA.Foldable)(
            pipe(
              query,
              O.getOrElseW(() => []),
            ),
            ({ name, value }) => [name, value],
          ),
        ),
      ),
      E.bindW("decodedQueryParams", ({ remappedQueryParams }) =>
        decodeQueryParams(route)(remappedQueryParams),
      ),
      E.map(
        ({ decodedRouteParams: routeParams, decodedQueryParams: queryParams }) =>
          ({ routeParams, queryParams } as RouteDecodeSuccess<R>),
      ),
    )

type QueryParamFailures = {
  readonly tag: "QueryParamFailures"
  readonly failures: RNEA.ReadonlyNonEmptyArray<
    MissingRequiredQueryParam | FailedToDecodeQueryParam
  >
}
type MissingRequiredQueryParam = {
  readonly tag: "MissingRequiredQueryParam"
  readonly queryParam: string
}
type FailedToDecodeQueryParam = {
  readonly tag: "FailedToDecodeQueryParam"
  readonly queryParam: string
  readonly error: DecodeError
}

const decodeQueryParams =
  <
    RSs extends Array<string>,
    RPs extends Partial<Record<string, RouteCodec<any>>>,
    QPs extends QueryParamCodecs<any>,
    R extends RouteDefinition<RSs, RPs, QPs>,
  >(
    route: R,
  ) =>
  (
    remappedQueryParams: Readonly<Record<string, string>>,
  ): E.Either<QueryParamFailures, Readonly<Record<string, any>>> =>
    pipe(
      route.queryParamCodecs,
      RR.traverseWithIndex(
        E.getApplicativeValidation(
          RNEA.getSemigroup<MissingRequiredQueryParam | FailedToDecodeQueryParam>(),
        ),
      )<string, QueryParamCodec<any>, any>((key, codec) =>
        pipe(
          remappedQueryParams,
          RR.lookup(key),
          O.matchW(
            () =>
              codec.tag === "required"
                ? E.left([
                    {
                      tag: "MissingRequiredQueryParam" as const,
                      queryParam: key,
                    },
                  ])
                : E.right(O.none),
            (paramValue) =>
              pipe(
                codec.tag === "required"
                  ? codec.codec.decode(paramValue)
                  : pipe(codec.codec.decode(paramValue), E.map(O.some)),
                E.mapLeft((error) => [
                  {
                    tag: "FailedToDecodeQueryParam" as const,
                    queryParam: key,
                    error,
                  },
                ]),
              ),
          ),
        ),
      ),
      E.mapLeft((failures) => ({ tag: "QueryParamFailures" as const, failures })),
    )

type SegmentCountMismatch = {
  readonly tag: "SegmentCountMismatch"
  readonly expectedSegments: ReadonlyArray<string>
  readonly actualSegments: ReadonlyArray<string>
}
type SegmentMismatch = {
  readonly tag: "SegmentMismatch"
  readonly expected: string
  readonly actual: string
}
type MissingDecoder = {
  readonly tag: "MissingDecoder"
  readonly routeParam: string
}
type FailedToDecodeRouteParam = {
  readonly tag: "FailedToDecodeRouteParam"
  readonly routeParam: string
  readonly error: DecodeError
}

const decodeRouteParams =
  <
    RSs extends Array<string>,
    RPs extends Partial<Record<string, RouteCodec<any>>>,
    QPs extends QueryParamCodecs<any>,
    R extends RouteDefinition<RSs, RPs, QPs>,
  >(
    route: R,
  ) =>
  (
    givenSegments: ReadonlyArray<string>,
  ): E.Either<
    SegmentCountMismatch | SegmentMismatch | MissingDecoder | FailedToDecodeRouteParam,
    Readonly<Record<string, any>>
  > =>
    pipe(
      route.routeSegments,
      E.fromPredicate(
        () => {
          // this is to prevent an issue when parsing the unique root route, "/".
          // The parser parses "/" as [''], but the expected segments are []
          if (
            givenSegments.length === 1 &&
            givenSegments[0] === "" &&
            route.routeSegments.length === 0
          ) {
            return true
          }
          return givenSegments.length === route.routeSegments.length
        },
        () => ({
          tag: "SegmentCountMismatch" as const,
          expectedSegments: route.routeSegments,
          actualSegments: givenSegments,
        }),
      ),
      E.chainW(
        flow(
          RA.zip(givenSegments),
          /*
          check each URL segment; if it doesn't start with a `:` then we can
          filter it out (hence the wither), but it can still be an error if it
          doesn't match. and we also want to track what error comes up for
          better debugging, so use Either
          */
          RA.wither(E.Applicative)(
            ([segment, value]): E.Either<
              SegmentMismatch | MissingDecoder | FailedToDecodeRouteParam,
              O.Option<readonly [string, unknown]>
            > => {
              if (segment.startsWith(":")) {
                const routeParam: keyof RPs & string = segment.slice(1)
                return pipe(
                  route.routeParamCodecs[routeParam],
                  // This doesn't seem to be nullable in practice, caller would
                  // have had to construct a bad `RouteDefinition`
                  E.fromNullable({
                    tag: "MissingDecoder" as const,
                    routeParam,
                  }),
                  E.chainW(({ decode }) =>
                    pipe(
                      decode(value),
                      E.map((v) => O.some([routeParam, v] as const)),
                      E.mapLeft((error) => ({
                        tag: "FailedToDecodeRouteParam" as const,
                        routeParam,
                        error,
                      })),
                    ),
                  ),
                )
              }
              return segment === value
                ? E.of(O.none)
                : E.throwError({
                    tag: "SegmentMismatch" as const,
                    expected: segment,
                    actual: value,
                  })
            },
          ),
          E.map(RR.fromFoldable(Sg.last<unknown>(), RA.Foldable)),
        ),
      ),
    )

/**
 * Returns a multiline pre-formatted string
 */
export const prettyPrintFailure = (failure: RouteDecodeFailure): string => {
  const indent = (lines: string) =>
    lines
      .split("\n")
      .map((line) => "  ".concat(line))
      .join("\n")

  const printQueryParamFailure = (
    queryParamFailure: MissingRequiredQueryParam | FailedToDecodeQueryParam,
  ): string => {
    switch (queryParamFailure.tag) {
      case "FailedToDecodeQueryParam":
        return [
          `Failure parsing query param "${queryParamFailure.queryParam}":`,
          indent(draw(queryParamFailure.error)),
        ].join("\n")
      case "MissingRequiredQueryParam":
        return `Missing required query param "${queryParamFailure.queryParam}"`
    }
  }

  const printParseError = ({ expected, input }: ParseError<string>): string => {
    const [before, after] = RA.splitAt(input.cursor)(input.buffer).map((chs) => chs.join(""))

    // Keep the print-width to 80 chars
    const beforeTruncated = before.length <= 40 ? before : before.slice(-40).replace(/^./, "…")
    const _truncated = beforeTruncated.concat(after)
    const truncated =
      _truncated.length <= 80 ? _truncated : _truncated.slice(0, 80).replace(/.$/, "…")

    return [
      "Failure parsing URL:",
      indent(truncated),
      indent([" ".repeat(beforeTruncated.length), "⌃ Expected ", expected.join(" or ")].join("")),
    ].join("\n")
  }

  switch (failure.tag) {
    case "FailedToParseURL":
      return printParseError(failure.error)
    case "SegmentCountMismatch":
      return `Expected (${failure.expectedSegments.length}) segments, received (${failure.actualSegments.length})`
    case "SegmentMismatch":
      return `Expected segment to match "${failure.expected}", received "${failure.actual}"`
    case "MissingDecoder":
      return `Missing decoder for route param "${failure.routeParam}"`
    case "FailedToDecodeRouteParam":
      return [
        `Failure parsing route param "${failure.routeParam}":`,
        indent(draw(failure.error)),
      ].join("\n")
    case "QueryParamFailures":
      return failure.failures.map(printQueryParamFailure).join("\n\n")
  }
}
