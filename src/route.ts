/* eslint-disable @typescript-eslint/no-explicit-any */
import type { RouteCodec } from "./routeCodec"
import type * as O from "fp-ts/Option"
import type { ReadonlyRecord } from "fp-ts/ReadonlyRecord"

//#region Type definitions

export type RouteParamsFromRoute<R extends Array<string>, Acc = never> = R extends [
  `:${infer Param}`,
  ...infer Rest,
]
  ? Rest extends Array<string>
    ? RouteParamsFromRoute<Rest, Acc | Param>
    : never
  : R extends [string, ...infer Rest]
  ? Rest extends Array<string>
    ? RouteParamsFromRoute<Rest, Acc>
    : never
  : never

export type RouteDefinition<
  R extends Array<string>,
  RPs extends Partial<Record<RouteParamsFromRoute<R>, RouteCodec<any>>>,
  QPs extends QueryParamCodecs<any>,
> = {
  tag: "RouteDefinition"
  routeSegments: R
  routeParamCodecs: RPs
  queryParamCodecs: QPs
}

export type RouteParametersForRoute<Route> = Route extends RouteDefinition<
  infer _R,
  infer RPs,
  infer _QPs
>
  ? { [K in keyof RPs]: RPs[K] extends RouteCodec<infer C> ? C : never }
  : never

export type RouteParameters<RPs extends Record<string, RouteCodec<any>>> = {
  [K in keyof RPs]: RPs[K] extends RouteCodec<infer C> ? C : never
}

export type QueryParametersForRoute<Route> = Route extends RouteDefinition<
  infer _R,
  infer _RPs,
  infer QPs
>
  ? QueryParametersForCodecs<QPs>
  : never

export type QueryParametersForCodecs<QPs extends QueryParamCodecs<any>> = {
  [K in keyof QPs]: QPs[K] extends OptionalQueryParamCodec<infer C>
    ? O.Option<C>
    : QPs[K] extends RequiredQueryParamCodec<infer C>
    ? C
    : never
}

//#endregion

// eslint-disable-next-line @typescript-eslint/ban-types
export const RootPath: RouteDefinition<[], {}, {}> = {
  tag: "RouteDefinition",
  routeSegments: [],
  routeParamCodecs: {},
  queryParamCodecs: {},
}

type SlashlessNonparameter<S> = S extends `:${string}`
  ? never
  : S extends `${string}/${string}`
  ? never
  : S

export type OptionalQueryParamCodec<T> = {
  tag: "optional"
  codec: RouteCodec<T>
}
export type RequiredQueryParamCodec<T> = {
  tag: "required"
  codec: RouteCodec<T>
}

export type QueryParamCodec<T> = OptionalQueryParamCodec<T> | RequiredQueryParamCodec<T>

export type QueryParamCodecs<T> = ReadonlyRecord<string, QueryParamCodec<T>>

export type OptionalQueryParamKeys<QPs extends QueryParamCodecs<any>> = {
  [K in keyof QPs]: QPs[K] extends OptionalQueryParamCodec<any> ? K : never
}[keyof QPs]

type Join<R extends Array<string>, Acc extends string = ""> = R extends [infer S, ...infer Rest]
  ? Rest extends Array<string>
    ? S extends string
      ? Join<Rest, `${Acc}/${S}`>
      : never
    : never
  : Acc

export const getRouteString: <
  R extends Array<string>,
  RPs extends Partial<Record<RouteParamsFromRoute<R, never>, RouteCodec<any>>>,
  QPs extends QueryParamCodecs<any>,
>(
  route: RouteDefinition<R, RPs, QPs>,
) => Join<R> = (route) =>
  ("/" + route.routeSegments.join("/")) as Join<typeof route["routeSegments"]>

export const path: <R extends string>(
  r: SlashlessNonparameter<R>,
) => <
  RR extends Array<string>,
  RPs extends Partial<Record<RouteParamsFromRoute<RR>, RouteCodec<any>>>,
  QPs extends QueryParamCodecs<any>,
>(
  basePath: RouteDefinition<RR, RPs, QPs>,
  // eslint-disable-next-line @typescript-eslint/ban-types
) => RouteDefinition<[...RR, R], RPs, {}> = (r) => (basePath) => ({
  ...basePath,
  routeSegments: [...basePath.routeSegments, r],
  queryParamCodecs: {},
})

export const param =
  <
    Name extends string,
    A,
    RPs extends Partial<Record<RouteParamsFromRoute<any, never>, RouteCodec<any>>>,
  >(
    name: Exclude<SlashlessNonparameter<Name>, keyof RPs>,
    codec: RouteCodec<A>,
  ) =>
  <R extends Array<string>, QPs extends QueryParamCodecs<any>>(
    path: RouteDefinition<R, RPs, QPs>,
    // eslint-disable-next-line @typescript-eslint/ban-types
  ): RouteDefinition<[...R, `:${Name}`], RPs & { [N in Name]: RouteCodec<A> }, {}> => ({
    ...path,
    queryParamCodecs: {},
    routeSegments: [...path.routeSegments, `:${name}`],
    routeParamCodecs: { ...path.routeParamCodecs, [name]: codec } as RPs & {
      [N in Name]: RouteCodec<A>
    },
  })

export const queryParam =
  <QPs extends QueryParamCodecs<any>, Name extends string, A>(
    name: Exclude<Name, keyof QPs>,
    codec: RouteCodec<A>,
  ) =>
  <
    R extends Array<string>,
    RPs extends Partial<Record<RouteParamsFromRoute<R, never>, RouteCodec<any>>>,
  >(
    path: RouteDefinition<R, RPs, QPs>,
  ): RouteDefinition<R, RPs, QPs & { [N in Name]: RequiredQueryParamCodec<A> }> => ({
    ...path,
    queryParamCodecs: {
      ...path.queryParamCodecs,
      [name]: { tag: "required", codec },
    },
  })

export const queryParamOp =
  <QPs extends QueryParamCodecs<any>, Name extends string, A>(
    name: Exclude<Name, keyof QPs>,
    codec: RouteCodec<A>,
  ) =>
  <
    R extends Array<string>,
    RPs extends Partial<Record<RouteParamsFromRoute<R, never>, RouteCodec<any>>>,
  >(
    path: RouteDefinition<R, RPs, QPs>,
  ): RouteDefinition<R, RPs, QPs & { [N in Name]: OptionalQueryParamCodec<A> }> => ({
    ...path,
    queryParamCodecs: {
      ...path.queryParamCodecs,
      [name]: { tag: "optional", codec },
    },
  })
