import type * as Eq from "fp-ts/Eq"
import type { Iso } from "monocle-ts"

import * as E from "fp-ts/Either"
import { flow, pipe } from "fp-ts/function"
import * as Mn from "fp-ts/Monoid"
import * as Num from "fp-ts/number"
import * as RA from "fp-ts/ReadonlyArray"
import * as Str from "fp-ts/string"
import * as C from "io-ts/Codec"
import * as D from "io-ts/Decoder"
import * as c from "parser-ts/char"
import * as P from "parser-ts/Parser"
import { run } from "parser-ts/string"

export declare type Literal = string | number | boolean

export type RouteCodec<A> = C.Codec<string, string, A>

export const stringRC: RouteCodec<string> = C.make(
  { decode: flow(decodeURIComponent, E.right) },
  { encode: flow((s) => encodeURIComponent(s)) },
)

export const dateRC: RouteCodec<Date> = C.make(
  {
    decode: (str) => {
      const date = new Date(decodeURIComponent(str))
      return isNaN(date.getTime()) ? E.left(D.error(str, "valid date string")) : E.right(date)
    },
  },
  {
    encode: (date) => encodeURIComponent(date.toISOString()),
  },
)

export const tupleRC = <A, B>(
  codecA: RouteCodec<A>,
  codecB: RouteCodec<B>,
): RouteCodec<readonly [A, B]> =>
  C.make(
    {
      decode: (str: string) =>
        pipe(
          run(str)(arrayP),
          E.mapLeft((_err) => D.error(str, "tuple")),
          E.chain(({ value: [a, b] }) =>
            pipe(
              E.Do,
              E.apS("a", codecA.decode(a)),
              E.apS("b", codecB.decode(b)),
              E.map(({ a, b }) => [a, b] as const),
            ),
          ),
        ),
    },
    {
      encode: ([a, b]) =>
        pipe([codecA.encode(a), codecB.encode(b)], RA.intersperse(","), Mn.concatAll(Str.Monoid)) +
        ",",
    },
  )

export const numberRC: RouteCodec<number> = C.make(
  {
    decode: (str) =>
      pipe(
        +decodeURIComponent(str),
        E.fromPredicate(
          (n) => !Number.isNaN(n),
          () => D.error(str, "number"),
        ),
      ),
  },
  {
    encode: (n) =>
      Object.is(-0, n)
        ? "-0"
        : // Using encodeURIComponent so we can support numbers like `1e+21`
          encodeURIComponent(n.toString()),
  },
)

export const newtypeRC = <N, A>(iso: Iso<N, A>, codec: RouteCodec<A>): RouteCodec<N> =>
  C.make(
    {
      decode: (str) => pipe(codec.decode(str), E.map(iso.wrap)),
    },
    { encode: (n) => pipe(n, iso.unwrap, codec.encode) },
  )

const arrayMemP = pipe(
  P.takeUntil((c: c.Char) => c === ","),
  P.chainFirst(() => c.char(",")),
  P.map(Mn.concatAll(Str.Monoid)),
)
const arrayP = pipe(P.many(arrayMemP), P.map(RA.fromArray))

export const arrayRC = <A>(codec: RouteCodec<A>): RouteCodec<ReadonlyArray<A>> =>
  C.make(
    {
      decode: (str: string) =>
        pipe(
          run(str)(arrayP),
          E.mapLeft((_err) => D.error(str, "Array")),
          E.chain(({ value }) => E.traverseArray(codec.decode)(value)),
        ),
    },
    {
      encode: (n) =>
        pipe(n, RA.map(codec.encode), RA.intersperse(","), Mn.concatAll(Str.Monoid)) +
        (n.length > 0 ? "," : ""),
    },
  )

export const literalRC =
  <A extends Literal>(codec: RouteCodec<A>, eq: Eq.Eq<A>) =>
  <Ls extends readonly [A, ...Array<A>]>(...literals: Ls): RouteCodec<Ls[number]> =>
    C.make(
      {
        decode: (str) =>
          pipe(
            str,
            codec.decode,
            E.chain((value) =>
              pipe(
                literals,
                RA.findFirst((l) => eq.equals(value, l)),
                E.fromOption(() =>
                  D.error(value, `${literals.map((lit) => JSON.stringify(lit)).join(" | ")}`),
                ),
              ),
            ),
          ),
      },
      { encode: (value) => pipe(codec.encode(value)) },
    )

export const stringLiteralRC = literalRC(stringRC, Str.Eq)
export const numberLiteralRC = literalRC(numberRC, Num.Eq)

export const booleanRC: RouteCodec<boolean> = pipe(
  stringLiteralRC("true", "false"),
  C.imap(
    (str) => str === "true",
    (bool) => (bool ? "true" : "false"),
  ),
)
