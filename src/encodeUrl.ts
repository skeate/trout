/* eslint-disable @typescript-eslint/no-explicit-any */
import type {
  OptionalQueryParamKeys,
  QueryParamCodecs,
  QueryParametersForCodecs,
  QueryParametersForRoute,
  RouteDefinition,
  RouteParameters,
} from "./route"
import type { RouteCodec } from "./routeCodec"

import { identity, pipe } from "fp-ts/function"
import * as O from "fp-ts/Option"
import * as RA from "fp-ts/ReadonlyArray"
import * as RNEA from "fp-ts/ReadonlyNonEmptyArray"
import * as RR from "fp-ts/ReadonlyRecord"
import * as Str from "fp-ts/string"

type RouteParamType<
  S extends string,
  RPs extends Record<string, RouteCodec<any>>,
> = RPs[S] extends RouteCodec<infer A> ? (A extends string ? A : string) : string

type UrlPath<
  R extends Array<string>,
  RPs extends Record<string, RouteCodec<any>>,
  Acc extends string = "",
> = R extends [infer S, ...infer Rest]
  ? Rest extends Array<string>
    ? S extends `:${infer K}`
      ? UrlPath<Rest, RPs, `${Acc}/${RouteParamType<K, RPs>}`>
      : S extends string
      ? UrlPath<Rest, RPs, `${Acc}/${S}`>
      : never
    : never
  : Acc

type UrlQueryParams<QPs extends QueryParamCodecs<any>> = keyof QPs extends never
  ? ``
  : [keyof QPs] extends [OptionalQueryParamKeys<QPs>]
  ? `?${string}` | ``
  : `?${string}`

type UrlFrom<
  R extends Array<string>,
  RPs extends Record<string, RouteCodec<any>>,
  QPs extends QueryParamCodecs<any>,
> = R extends [] ? `/${UrlQueryParams<QPs>}` : `${UrlPath<R, RPs>}${UrlQueryParams<QPs>}`

type ParamsFor<
  RPs extends Record<string, RouteCodec<any>>,
  QPs extends QueryParamCodecs<any>,
> = keyof (RPs & QPs) extends never
  ? // eslint-disable-next-line @typescript-eslint/ban-types
    {}
  : keyof QPs extends never
  ? { route: RouteParameters<RPs> }
  : keyof RPs extends never
  ? { query: QueryParametersForCodecs<QPs> }
  : { route: RouteParameters<RPs>; query: QueryParametersForCodecs<QPs> }

export type UrlTypeOf<RD> = RD extends RouteDefinition<infer RSs, infer RPs, infer QPs>
  ? RPs extends Record<string, RouteCodec<any>>
    ? UrlFrom<RSs, RPs, QPs>
    : never
  : never

export const encodeUrl =
  <
    RSs extends Array<string>,
    RPs extends Record<string, RouteCodec<any>>,
    QPs extends QueryParamCodecs<any>,
  >(
    route: RouteDefinition<RSs, RPs, QPs>,
  ) =>
  (params: ParamsFor<RPs, QPs>): UrlFrom<RSs, RPs, QPs> => {
    const routeParameters = "route" in params ? params.route : ({} as RouteParameters<RPs>)
    const queryParameters = "query" in params ? params.query : ({} as QueryParametersForCodecs<QPs>)
    const path =
      "/" +
      route.routeSegments
        .map((rs) => {
          if (rs.startsWith(":")) {
            const paramName = rs.replace(":", "")
            const value = routeParameters[paramName]
            const codec = route.routeParamCodecs[paramName]
            return codec.encode(value)
          } else {
            return rs
          }
        })
        .join("/")
    return (path + encodeQueryParams(route)(queryParameters)) as UrlFrom<RSs, RPs, QPs>
  }

type QueryParamCodecWithValue<T> =
  | {
      tag: "optional"
      name: string
      codec: RouteCodec<T>
      value: O.Option<T>
    }
  | {
      tag: "required"
      name: string
      codec: RouteCodec<T>
      value: T
    }

const encodeQueryParams =
  <
    RSs extends Array<string>,
    RPs extends Record<string, RouteCodec<any>>,
    QPs extends QueryParamCodecs<any>,
    R extends RouteDefinition<RSs, RPs, QPs>,
  >(
    route: R,
  ) =>
  (queryParameters: QueryParametersForRoute<R>): string => {
    // collect codecs with their values to make encoding easier
    const values: ReadonlyArray<QueryParamCodecWithValue<unknown>> = pipe(
      queryParameters,
      RR.collect(Str.Ord)((k, v) => {
        if (route.queryParamCodecs[k].tag === "optional") {
          // assume v is an O.Option value
          return {
            tag: "optional",
            value: v as O.Option<unknown>,
            codec: route.queryParamCodecs[k].codec,
            name: k,
          }
        } else {
          return {
            tag: "required",
            value: v,
            codec: route.queryParamCodecs[k].codec,
            name: k,
          }
        }
      }),
    )
    return encodeQueryParamValues(values)
  }

const encodeQueryParamValues = (
  paramsWithCodecs: ReadonlyArray<QueryParamCodecWithValue<unknown>>,
): string =>
  pipe(
    paramsWithCodecs,
    RA.filterMap((qpwc) =>
      pipe(
        encodeQueryParamValue(qpwc),
        O.map((v) => `${qpwc.name}=${v}`),
      ),
    ),
    RA.intersperse("&"),
    RNEA.fromReadonlyArray,
    O.fold(
      () => "",
      (s) => "?" + pipe(s, RA.foldMap(Str.Monoid)(identity)),
    ),
  )

const encodeQueryParamValue = (
  codecWithValue: QueryParamCodecWithValue<unknown>,
): O.Option<string> => {
  if (codecWithValue.tag === "optional") {
    return pipe(codecWithValue.value, O.map(codecWithValue.codec.encode))
  } else {
    return O.some(codecWithValue.codec.encode(codecWithValue.value))
  }
}
